# WHAT IS NETWORKING
A Computer network refers two or computers that can exchange information over a data link.

## WHAT ARE NETWORK MADE OF
Any device that connects to a network is called a node.  E.g computers, tablets, switches.   
A network can be made of the following;   
- End Devices 
  - Computers
  - Laptops
  - Servers
  - Phones
  - Tablets      
     
- Infrastructure Devices
  - Routers
  - Switches
  - Firewalls
  - Hubs
  - Wireless Access Points

- Cable and Media  
  - Fiber Optics
  - Cat 5e cable
  - Coaxial cable
  - Phone Lines
  - Wireless Radio

- Protocols  
  - Internet Protocol (IP)
  - Transmission Control Protocol (TCP)
  - User Datagram Protocol (UDP)  
The Transmission Control Protocol/Internet Protocol (TCP/IP) is the
primary protocol of most modern networks, including the Internet. For a
computing device to access the Internet, it must have TCP/IP loaded and
configured properly. 


## WHAT IS THE PURPOSE OF A NETWORK
- Connects computers together
- Provides access to network services
- Files sharing and data access
- Email and messaging
- Printing and Faxing
- Phone service (VOIP)
- Video streaming
- Internet and Web access
- Wired and Wireless access  

## BASIC NETWORK COMPONENT
- Client
- Server
- Hub
- Switch 
- Router
- Wireless Access Point
- Media

  

### CLIENT
- A Workstation or other end user device that provides a user with access to the network.   
- includes a PC, laptops, tablet, smartphone, teleconferencing endpoint and VoIP phone  
- Client uses the services of another computer or the shared resources of a network server.  


SERVER  
- A computer that shares its resources with other computers on the network.
- e.g File Server, Print Server, Email Server and Web Server  
- Servers can be local to the network or remote.  

HUB and SWITCH  
- The primary purpose of the both Hub and Switch is to provide wired access to a LAN.   

- Hubs are also called repeater because they repeat the recieved frames out of every port  


- Switches  are intelligent. Switches know which devices are connected to them by learning the MAC address of each device.  
- Switches keeps a table of all connected MAC addresses and only send frames to the intended destination whereas Hubs flood the entire network. In this way Switches provide much more efficiency and better utilization of the network.


WIRELESS ACCESS POINT
 - Also known as WAP, Wireless AP or Access Point  
- WAP Connects wireless network nodes to wireless or wired networks.
 - Many commercial grade WAP are specifically for wireless access only
 - Most consumer grade WAPs and some commercial grade WAPs are combination devices that also act as a switch and router.  

 ROUTERS
 - A devices that connects separate Network 
 - Intelligently forwards packets from one network to another based on the network address such as the Internet Protocol (IP) address.
 Router is the Gateway to leave the LAN such as when routing traffic to the internet or to the another LAN.  

 NETWORK MEDIA
 - Media is what physically connects a device into the network .
 - Media can be wired (Cables) or wireless (radio waves). Wired media comes in the form of copper cables and fiber optic cables
 - Both wired and wireless media have many different standards associated with them. Some examples are distance limitations and bandwith/speed capabilities.


 ## TYPES OF NETWORK
- LAN (Local Area Network)
- MAN (Metropolitan Area Network)
- WAN (Wide Area Network)
- WLAN (Wireless LAN)
- WWAN (Wireless WAN)
- SAN (Storage Area Network)
- CAN (Campus Area Network)
- PAN (Personal Area Network)  


Two of the most common types of network  are LAN and WAN   

LAN connects devices in small geographic location, such as building, office and home.   
LAN use the Ethernet standard for connecting devices, and they usually have a high data transfer rate.

WAN is a collection of local-Area Networks (LANs) over long distances such cities or countries
WAN use technology such as fiber-optic cables and satellites to transmit data.
Internet is an example of a WAN and it's considered the largest WAN  

## IP ADDRESS

The IP address is the unique identification number for your system on the
network. Most systems today rely on the Internet Protocol version 4 (IPv4)
addressing scheme. IPv4 addresses consist of four sets of eight binary
numbers (octets), each set separated by a period. This is called dotteddecimal
notation.  
- Example: 192.168.100.1
- 4 binary Octets: 11000000.10101000. 01100100.0000001 
- IP address identifies the node and
the network on which it resides.

## SUBNET MASK  
NIC uses a value called the subnet mask to distinguish which part of the IP
address identifies the network ID and which part of the address identifies the
host. The subnet mask blocks out (or masks) the network portion of an IP
address.
Let’s look at a typical subnet mask: 255.255.255.0. When you compare
the subnet mask to the IP address, any part that’s all 255s is the network ID.
Any part that’s all zeros is the host ID.



### PUBLIC VS PRIVATE
- Public IP address can be accessed over the internet.
- A private IP address is assingned to computers within a private network  and they cannot be accessed from the internet.  
- The RFC1918 address space includes the following networks:  
 10.0.0.0 – 10.255.255.255  (10/8 prefix)  
172.16.0.0 – 172.31.255.255  (172.16/12 prefix)  
192.168.0.0 – 192.168.255.255 (192.168/16 prefix)


### MAC ADDRESS
- MAC addresss is made up of 12 Hexadecimal digits
- Each Hexadecimal digit is 4 binary bits
- 48 bits total length.
- That is 48 bits unique identifier for a network interface card
- Sometimes referred to as Hardware address, Physical address or burned-in address
- The first half of MAC address represent the OEM (Original Equipment Manufacturer)
- The last half represent unique ID
- To check the manufacturer of your network interface card, refer to [www.macvendors.com](https://www.macvendors.com)
- To check MAC address on your device   
  -  Windows  
   ipconfig/all  
  -  Mac/ Linux  
   ifconfig


BITS VS BYTES
- Bits and Bytes are both units of data. A bit is considered to be the smallest unit of data measurement. A bit can be either 0 or 1
- Network data rate are measured in bits per second
- There are 8 bits in a Bytes
- Files sizes are measured in Bytes
- Network devices send and receives data in chunks called Frames (sometimes called packet)
- Ethernet frames is 1500 Bytes in length (payload)
- The life of a frame begins and ends inside the network interface card















