# Intro to Security
## CIA Triad

C: Confidentiality: data only available to authorized parties.
I: Integrity: certainty that data is not compromised during or after submission.
A: Availability: data available to authorized users when needed.

## Types of Theft
1. Identity Theft: an event that targets personally identifiable info (PII), such as:
* name
* birthday
* password
* bank account/credit card numbers
2. Data Theft:  an event that targets confidential company data or intellectual property; thief may try to sell data or exact ransom to return to owner.
> As a result of data theft, organizations may lose ability to conduct business, give rival competition edge over business, and result in loss of public trust and reliability.

## Types of Threats
1. Malware: software designed to disrupt, damage, or gain unauthorized access to computer.
2. Password events: recovering passwords from data that has been stored by CPU.
3. Distributed Denial of Service (DDoS): multiple compromised systems used to compromise single system.
4. Man in the Middle: outside party secretly relays communication between two parties.
5. Phishing: outside party sends email pretending to be legitimate company to retrieve personal information (passwords, credit cards, etc)
6. Social Engineering: event that uses social interaction to manipulate a person to break security procedures.
7. Drive By: unsecure websites that plant malicious codes.

## Types of Security
1. Physical security
2. Access management
3. System security
4. Infrastructure security
5. Identity management
6. Data security
7. Software security

## Security Life Cycle
Prevention > Detection > Response > Analysis
> More about security life cycle [here](https://excalidraw.com/#json=k5oBJh9Z1At9kzuIYiwuN,bGicyR6bJMLlTW2up8HHwg)
>> Prevention
>>
>> Defense in Depth:  best defense approach; implement several layers of security that an outsider must penetrate to gain access

## Open Systems Interconnection (OSI)
Seven layers of computer communication over a network
1. Physical
2. Data
3. Network
4. Transport
5. Session
6. Presentation
7. Application

## Layered Security Model
Each layer offers different level of defense for assets
1. Perimeter security
* Perimeter firewalls
* Intrusion Detection System (IDS) and Intrusion Prevention System (IPS)
* Secure the perimeter networks
2. Network security
* Network Access Control (NAC)
* Enterprise IDS or IPS
* Web proxy content filtering
3. Endpoint security
* Desktop firewall
* Host IDS or IPS
* Content security (anti-virus)
4. App security
* Dynamic app testing
* Web app firewall (WAF)
* Database monitoring
5. Data security
* Identity and access management
* Data wiping cleanses
* Data loss prevention (DLP)

## Prevention
Hardening: reduce the number of running services on a system; balance between security and usability.

Common ways to harden:
* Turn off unnecessary services
* Control computer operations through group policies
* Regularly apply patches and updates

Patching: applied on a system where weakness is discovered
* Fixes performance and feature issues
* Reduces methods that can infiltrate system to make it more reliable and secure
* Comes as a software update

## AAA Security Facet
* Authentication: validates identity
* Authorization: verifies access permission
* Accounting: gathers usage and other info used for auditing and billing

## Network Based Intrusion Detection System (NIDS)
Monitors network activity and uses signatures/rules to detect malicious patterns.
* Signature based detection: compares network traffic against a database of known malicious patterns
* Behavior based detection/anomaly based detection: uses a set of rules or known baseline to detect anomalies

## Host Based Intrusion Detection System
Protects critical files and prevents behavior changes due to corrupt computer state; complements anti-virus software.
* Backdoor threat: vulnerability that allows an attacker to bypass normal security checks and access a system
* Rootkit threat: vulnerability that allows an attacker to access a system as a root user in an undetected fashion

## Firewalls
Allow or disallow traffic between hosts or networks
* Network firewall filters packets between two subnets
* Host firewall filters packets directed to the host

## Network Zones
Designated area with common security properties.  Can be:
* Fully controlled (intranet)
* Partially controlled (perimeter zone)
    > Perimeter zone: serves as a buffer between two zones with different trust levels
   >> * Back to back configuration: between two firewalls
   >> * Three leg configuration: behind the same firewall that protects the internal network
* Uncontrolled (internet)

## Network Address Translation (NAT) vs. Port Address Translation (PAT)
NAT:  translates private addresses to public addresses.

PAT: associates multiple private addresses with one public address.

## Network Access Control Lists (ACLs)
Inspects systems that are connected to protected segments to verify compliance and security policy.

Network ACLs can:
* Verify anti-virus
* Make sure firewall is enabled
* Verify anti-spyware

## AWS Security Groups 
Security groups: act as built in firewall for your virtual servers (virtual firewall).  Owner has full control over how accessible instances are.

Security groups are stateful, network ACLs are stateless.
* Stateful: the computer tracks the state of interaction, usually by setting values in storage setting
* Stateless: no info is retained by the sender or receiver

## Cryptography
The discipline that embodies the principles and techniques for providing data security, including confidentiality and data integrity.

## Encryption
Achieves data confidentiality by use of:
* Cryptographic algorithms to cipher data
* A secret key to dicipher data

Types of encryption:
* Symmetric: uses same key to encrypt and decrypt data (fastest)
* Asymmetric: uses both a publc key and private key to encrypt and decrypt data (complex & slower)
* Hybrid: uses both symmetric and asymmetric encryption to further protect data
    > Used by Transmission Layer Security (TLS) and Secure Sockets Layer (SSL)

## Hashing
Used to ensure data integrity; a hash function generates a **unique hash value** or **message digest** from the content of a file or message.

Recipients of the file/message can use the hash value to verify that the content has not changed during transit.

## Discretionary Access Control (DAC) vs. Role Based Access Control
Discretionary Access Control:
* Decentralized access
* Access Control Lists (ACLs)
* Access Control Entities (ACEs)
* Auditing
* Dynamic access control

Role Based Access Control:
* A level of access to a resource is assigned to a role; permissions distributed based on role.

## Public Key Infrastructure (PKI)
A collection of technologies that are used to apply cryptography principles based on practical implementation of keys

Enables:
* Confidentiality, integrity, nonrepudiation and authenticity
* Management of trust and relationships

Core components:
* Certificate authorities (CAs)
    > Issues certificates to entities; also manages trust relationships
* Certificates
* Revocation lists
* Registration authorities (RAs)
* Entities
* Certificate templates

Take a deeper look at PKIs [here](https://www.keyfactor.com/resources/what-is-pki/)

## Root CAs vs. Subordinate CAs
Root CA: top of hierarchy, stores the root CA keys.  Not directly involved in serving entities (standalone).

Subordinate CA: known as registration authority; verifies and validates entity requests and issues certificates to entities.

## Internal CAs vs. External CAs
Internal CA: fully configured by an organization, not trusted in outside world.  More administrative overhead.

External CA: configured by third party provider, no direct access granted.  Used for generating certificates that will be trusted by everyone.
>   Digital certificate: electronic credential that is used to represent the online identity of an individual, computer, or other entity.

## Identity Management
Actively administers subjects, objects, and their access permissions.

Ensures that identities receive appropriate access to resources and that systems remain scalable in granting access to resources.

Authentication Factors:
* Something you know (passwords, PINs, etc)
* Something you have (smartcard, certification, USB key)
* Something you are (fingerprint, face ID)

## Malware
Malicious software (malware) is designed to cause harm to a computer system by interrupting one of the CIA triad elements.

### Types of Malware
* Viruses: attaches to system applications and runs every time a program runs
* Worms: allows author to control infected computer remotely, spreads quickly
* Bots: control computers or launch distributed denial of service (DDoS) attacks against vulnerable systems
* Backdoors (Trojan Horse): secret server that steals info from victims system
* Rootkits: cloaks itself by replacing system files that can reveal its presence; can become part of operating system
* Spyware: jeopardizes privacy and typically comes embedded into applications that can look free and interesting to use
* Adware/Scareware: deploys ad content and monitors user activity like visited sites; often included in shareware apps
* Ransomware: locks systems or makes data unavailable until user pays

